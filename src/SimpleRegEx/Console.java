package SimpleRegEx;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Console {

	public static void main(String[] args) {
		// simple input check with Pattern.matches()
		System.out.println("(a|b) \t\t a \t\t" + Pattern.matches("(a|b)", "a"));
		System.out.println("(a|b).* \t a… \t\t" + Pattern.matches("(a|b).*", "aXXXXos7983e"));
		// only true, if (whole) string matches!
		System.out.println("(a|b).* \t x…a \t\t" + Pattern.matches("(a|b).*", "xxxxa"));
		
		// Attention: Backslash needs to be escaped too!
		System.out.println("(.+).com \t domain.com \t" + Pattern.matches("(.+)\\.com", "domain.com"));
		
		System.out.println();
		
		final String regex = "^([^@ ]+)@([^@ ]+?)(\\.[^@ ]{2,})$";
		final String string = "user.name@example.com\n"
				+ "neuer-nutzer@good.com";

		// modifiers/flags!
		final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
		// or:
//		final Pattern patternAlt = Pattern.compile("(?m)" + regex); // not recommended
		
		// find matches in given string
		final Matcher matcher = pattern.matcher(string);

		// iterate…
		while (matcher.find()) {
		    System.out.println("Full match: " + matcher.group(0));
		    
		    for (int i = 1; i <= matcher.groupCount(); i++) {
		        System.out.println("Group " + i + ": " + matcher.group(i));
		    }
		}

	}
	
//	private static void regExConsole(String regex, String input) {
//		boolean matches = Pattern.matches(regex, input);
//		System.out.printf("%s \t %s \t\t %s\n", regex, input, matches);
//	}

}
