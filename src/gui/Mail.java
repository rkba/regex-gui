package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

import javax.swing.*;

public class Mail {
	final static String MAIL_REGEX = ".+@.+\\..+";
	
	public static void main(String[] args) {
		JFrame f = new JFrame( "Mail" );
		
		final Font globalFont = new Font("", Font.BOLD, 30);
//		f.setFont(globalFont);

		// input
		final JTextField input = new JTextField( "", 20 );
		input.setFont(globalFont);
		input.setMaximumSize(new Dimension(1000, 30));
		input.setAlignmentX(Component.CENTER_ALIGNMENT);
//		input.setPreferredSize(new Dimension(600, 30));
		
	    f.add( input );
	    
	    // label
	    final JLabel label = new JLabel(" ");
	    label.setFont(globalFont);
	    label.setAlignmentX(Component.CENTER_ALIGNMENT);
	    
	    f.add(label);
	    
	    // button
	    final JButton button = new JButton( "Check" );
	    button.setFont(globalFont);
	    button.setAlignmentX(Component.CENTER_ALIGNMENT);
	    button.setAlignmentY(Component.BOTTOM_ALIGNMENT);
//	    button.setLocation(15, 15);
//	    button.setSize(200, 25);

	    f.add( button );

	    // on click
	    button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String text = input.getText();
				if (Pattern.matches(Mail.MAIL_REGEX, text)) {
					label.setText("Is valid mail");
					label.setForeground( Color.GREEN );
				    
				} else {
					label.setText("Invalid mail. Please enter a valid mail!");
					label.setForeground( Color.RED );
				}
				
			}
		});
	    
	    // show window
	    f.setLayout( new BoxLayout(f.getContentPane(), BoxLayout.Y_AXIS) );
	    f.pack();
	    f.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

//	    f.setMinimumSize(new Dimension( 600, 100 ));
	    
	    f.setLocationByPlatform(true);

	    f.setVisible( true );

	}

}
